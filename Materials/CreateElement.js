/*
  CREATE ELEMENT
  document.createElement(tag) – создает элемент
  document.createTextNode(value) – создает текстовый узел

  PASTE ELEMENT
  parent.appendChild(element)
  parent.insertBefore(element, nextSibling)

  REMOVE ELEMENT
  parentElement.removeChild(element);
  element.remove();

*/
var createArea = document.getElementById('createArea');

    var div = document.createElement('div');
    var textElem = document.createTextNode('Тут был я');
    div.className = "message";
    div.innerText = "status";
    div.style.backgroundColor = 'red';
    console.log(div);

    // createArea.insertBefore(textElem, createArea.children[1]);

var deletedElement = document.getElementById('JackLi');
// console.log( deletedElement );
//     deletedElement.remove();
var list = document.getElementById('list');
    list.removeChild(deletedElement);
